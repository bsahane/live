## Topic

Remove Mailing List but keep Archive on Public Link

## Environment(s)

## Input

|  Arguments   |                                                                            Options                                                                             |
|--------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Requester    | <span id="text-box"><input class="textbox-value md-input" type="text" name="ln_requester" placeholder="$requester" onkeyup="ln_requester(this);"></span>       |
| Mailing List | <span id="text-box"><input class="textbox-value md-input md-input--stretch mdx-iconsearch__input" type="text" name="ln_mailingList" placeholder="$mailingList" onkeyup="ln_mailingList(this);"></span> |

## Solution

#### Step 1: Verification

- Verify approvals from all administrators/owners of the list
- Check user <span id="requester11">$requester</span> using

<div><pre><code>ldapsearch -b ou=mx,dc=redhat,dc=com sendmailMTAKey=<span id="mailingList21">$mailingList</span></code></pre></div>


- Here we found information regarding Mailing Server for given mailing list

- verify mailing list
  
<div><pre><code>ls -l <span id="mailingList22">$mailingList</span></code></pre></div>

- Here we found archive and files regarding mailing list

- Now verify mailing list owners using

<div><pre><code>list_owners <span id="mailingList23">$mailingList</span></code></pre></div>

- and mailing list members using

<div><pre><code>list_members <span id="mailingList24">$mailingList</span></code></pre></div>

```bash
echo "Hello World!"
```